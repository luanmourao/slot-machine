let imgPath = './assets/';
let imgList = []; 

for (let i = 1; i <= 5; i++){
  imgList[i] = `${imgPath}${i}.png`;
  new Image().src = imgList[i]; 
}

document.getElementById("stop").classList.add("disabled");

let playButton = document.getElementById("play").onclick = function(){

  document.getElementById("play").classList.add("disabled");

  document.getElementById("stop").classList.remove("disabled");

  document.getElementById("stop").removeAttribute("disabled"); 

  document.getElementById("outResult").textContent = "";

  document.getElementById("play").setAttribute("disabled", "disabled");  

  document.getElementById("img1").classList.remove("imgBorder");
  document.getElementById("img2").classList.remove("imgBorder");
  document.getElementById("img3").classList.remove("imgBorder");

  let random = setInterval(function(){

    let top = Math.ceil(Math.random() * 5);
    let middle = Math.ceil(Math.random() * 5);
    let bottom = Math.ceil(Math.random() * 5);

    document.top.src = imgList[top]; 
    document.middle.src = imgList[middle];
    document.bottom.src = imgList[bottom]; 

      document.getElementById("stop").onclick = function(){
        clearInterval(random); 

        document.getElementById("stop").classList.add("disabled");
        document.getElementById("play").classList.remove("disabled");

        document.getElementById("img1").classList.add("imgBorder");
        document.getElementById("img2").classList.add("imgBorder");
        document.getElementById("img3").classList.add("imgBorder");
        
        document.getElementById("stop").setAttribute("disabled", "disabled");  
        document.getElementById("play").removeAttribute("disabled"); 
        
          let finalTop = imgList[top];
          let finalMiddle = imgList[middle];
          let finalBottom = imgList[bottom];
    
          if (finalTop === finalMiddle && finalTop === finalBottom){
            document.getElementById("outResult").textContent = "Parabéns, você conseguiu !!!";
          } else if (finalTop === finalMiddle || finalTop === finalBottom || finalMiddle === finalBottom){
            document.getElementById("outResult").textContent = "Quase! Tente novamente...";
          } else {
            document.getElementById("outResult").textContent = "Ah, que pena. Tente novamente...";
          }
    
    }

  }, 100);

}